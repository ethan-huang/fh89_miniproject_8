use std::error::Error;
use std::fs::File;
use csv::ReaderBuilder;

// Define a struct to represent a data source
#[derive(Debug, PartialEq)]
struct DataSource {
    source: String,
    url: String,
    year: String,
    file: String,
}

impl DataSource {
    // Function to create a new data source from CSV record
    fn new(record: csv::StringRecord) -> DataSource {
        DataSource {
            source: record.get(0).unwrap_or("").to_string(),
            url: record.get(1).unwrap_or("").to_string(),
            year: record.get(2).unwrap_or("").to_string(),
            file: record.get(3).unwrap_or("").to_string(),
        }
    }
}

// Function to read data sources from CSV and return vector of data sources
fn read_data_sources_from_csv(file_path: &str) -> Result<Vec<DataSource>, Box<dyn Error>> {
    let mut data_sources = Vec::new();
    let file = File::open(file_path)?;
    let mut rdr = ReaderBuilder::new().from_reader(file);
    for result in rdr.records() {
        let record = result?;
        let data_source = DataSource::new(record);
        data_sources.push(data_source);
    }
    Ok(data_sources)
}

// Function to find data source by source (case-sensitive)
fn find_data_source_by_source<'a>(data_sources: &'a [DataSource], source: &str) -> Option<&'a DataSource> {
    data_sources.iter().find(|ds| ds.source == source)
}

fn main() {
    // Read data sources from CSV
    let data_sources = match read_data_sources_from_csv("./data/data.csv") {
        Ok(sources) => sources,
        Err(err) => {
            eprintln!("Error reading data sources: {}", err);
            return;
        }
    };

    // Get source from command line argument
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        eprintln!("Usage: {} <source>", args[0]);
        return;
    }
    let source = &args[1];

    // Find data source by source and print information
    match find_data_source_by_source(&data_sources, source) {
        Some(source) => println!("{:?}", source),
        None => println!("Data source not found for source {}", source),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_find_data_source_by_source_existing() {
        let data_sources = vec![
            DataSource {
                source: "Metacritic".to_string(),
                url: "https://www.metacritic.com/browse/games/score/metascore/all/all/filtered".to_string(),
                year: "2022".to_string(),
                file: "metacritic-2022.csv".to_string(),
            },
            DataSource {
                source: "Opencritic".to_string(),
                url: "https://opencritic.com/browse/all".to_string(),
                year: "2020".to_string(),
                file: "opencritic-2020.csv".to_string(),
            },
        ];

        assert_eq!(
            find_data_source_by_source(&data_sources, "Metacritic"),
            Some(&DataSource {
                source: "Metacritic".to_string(),
                url: "https://www.metacritic.com/browse/games/score/metascore/all/all/filtered".to_string(),
                year: "2022".to_string(),
                file: "metacritic-2022.csv".to_string(),
            })
        );
    }

    #[test]
    fn test_find_data_source_by_source_not_existing() {
        let data_sources = vec![
            DataSource {
                source: "Metacritic".to_string(),
                url: "https://www.metacritic.com/browse/games/score/metascore/all/all/filtered".to_string(),
                year: "2022".to_string(),
                file: "metacritic-2022.csv".to_string(),
            },
        ];

        assert_eq!(find_data_source_by_source(&data_sources, "Edge"), None);
    }
}
