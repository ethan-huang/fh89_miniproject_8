## IDS721miniProject 8

- This project demonstrates the process of working with a vector database using Rust. And I implemented code for Rust command line tool, data processing and unit tests.

- My database is a csv file which contains fields: source, year, file, and url. The data type are all string.

## Build Process

- `cargo new <project> --bin` to start a new project. The bin flag is able to build the command-line tool later.
- add dependencies to the Cargo.toml file:
  csv = "1.1.6"
- add code into main.rs for data search and unit tests.

## Tests functionality

- `cargo build` to build the whole project
- `./target/debug/mini8 <source_name>` to test the functionality
- If the source name exists in the csv file, then it will show the detailed information corresponding to that data point. If it does not exist, then it will show "Data source not found for source <source_name>"

- ![result](/images/result.png)

- ![csv](/images/csv.png)

# Test Report

- Employed Rust framwork to do `cargo test` to validate my search function can work properly.

## Test Cases

The unit tests focus on validating the functionality of the `find_data_source_by_source` function. I created two sample data point, and searched for the correct source(case-sensitive). The detailed information should appear. Also, if the source does not exist in this two data points, it should trigger an "alert" to let users know that the source does not exist in the database.

1. **Existing Character**: Tests if the function correctly finds an existing data_source by source.
2. **Non-Existing Character**: Tests if the function returns `None` when the source does not exist in the provided vector.

## Results

All of the tests passed successfully, which implies that all of the functions can work properly.

1. **Test Case: Existing source**
   - **Outcome**: Passed
   - **Description**: The function correctly identified an existing data_source by source.
2. **Test Case: Non-Existing source**
   - **Outcome**: Passed
   - **Description**: The function appropriately returned `None` when the data_source was not found in the vector.

## Conclusion

The unit test is similar to the real-life scenario searching. The provided unit tests demonstrate this by testing the find_data_source_by_source function with different scenarios, including both existing and non-existing data sources, which shows that the functionality of searching function are robust and reliable.
